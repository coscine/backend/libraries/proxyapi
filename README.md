## ProxyApi Library

[[_TOC_]] 

## 📝 Overview

The ProxyApi library for .NET enables access to the RWTHAppProxy and OAuth2 of the RWTH Aachen University within the Coscine project, providing a set of helper classes for token verification and access to specific API methods, helping to streamline communication with the RWTH infrastructure and improve overall productivity.

## ⚙️ Configuration

To install this library, you can use the NuGet package manager or you can install it using the .NET CLI.
```powershell
Install-Package Coscine.ProxyApi
```
or using the .NET CLI
```powershell
dotnet add package Coscine.ProxyApi
```

Furthermore some helper classes exists which can verify tokens for accessing certain Api methods. The following configuration keys are currently settable:

* `coscine/global/context/metadata/scope`
* `coscine/global/context/metadata/serviceid`
* `coscine/global/context/coscine/scope`
* `coscine/global/context/coscine/serviceid`
* `coscine/local/proxytest/refreshtoken` <= A valid refresh token for testing
* `coscine/global/epic/prefix`
* `coscine/global/epic/url`
* `coscine/global/epic/user`
* `coscine/global/epic/password`

## 📖 Usage

For examples on how we use this library, look into the source code of the following open-source [Coscine APIs](https://git.rwth-aachen.de/coscine/backend/apis) found in our public GitLab repository:
- `Project`
- `Resources`
- `User`  
... and others.

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!