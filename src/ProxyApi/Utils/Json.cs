﻿using System.IO;

namespace Coscine.ProxyApi.Utils
{
    public class Json : IJson
    {
        public string Serialize<T>(T t)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(t);
        }

        public void Serialize<T>(T t, Stream s)
        {
            var text = Serialize<T>(t);
            StreamWriter writer = new StreamWriter(s);
            writer.Write(text);
            writer.Flush();
        }

        public T Deserialize<T>(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return default(T);
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(s);
        }

        public T Deserialize<T>(Stream s)
        {
            using (var reader = new StreamReader(s))
            {
                var text = reader.ReadToEnd();
                return Deserialize<T>(text);
            }
        }
    }
}
