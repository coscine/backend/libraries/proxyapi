﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Coscine.ProxyApi.Utils
{
    public class OAuth2Client
    {
        private RestClient restClient = new RestClient();

        public OAuth2ClientContext GetParsedContextFromToken(string token, string scope, string serviceId, string resource, string method, string cost = null)
        {
            Uri oauthEndpoint = new Uri(Settings.OAuth2ContextUrlJson);

            string body = string.Format("service_id={0}&service_scope={1}&access_token={2}&resource={3}&method={4}&cost={5}", serviceId, scope, token, resource, method, cost);

            var context = restClient.HttpPostTextOAuth(oauthEndpoint, body);

            var result = JsonConvert.DeserializeObject<OAuth2ClientContext>(context);
            return result;
        }
    }

    public class OAuth2ClientContext
    {
        public string status { get; set; }

        public bool isValid { get; set; }

        public string appId { get; set; }

        public string legacyContext { get; set; }


        public string userId { get; set; }

        public List<string> userIds { get; set; }

        public string matNo { get; set; }

        public string gguid { get; set; }

        public List<string> gguids { get; set; }

        public string id { get; set; }

        public List<string> ids { get; set; }

        public List<string> groups { get; set; }

        public string courseId { get; set; }

        public string realm { get; set; }
    }
}
