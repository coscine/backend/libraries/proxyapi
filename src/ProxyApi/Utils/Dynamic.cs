﻿using System.Collections.Generic;
using System.Linq;

namespace Coscine.ProxyApi.Utils
{
    public class Dynamic : ServiceObject
    {
        private Dictionary<string, object> SubTree { get; set; }

        public Dynamic()
        {
            SubTree = new Dictionary<string, object>();
        }

        public object this[string key]
        {
            get
            {
                return SubTree[key];
            }
            set
            {
                SubTree[key] = value;
            }
        }

        public Dictionary<string, object> ToDictionaryDeep()
        {
            return SubTree.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value is Dynamic ? (kvp.Value as Dynamic).ToDictionaryDeep() : kvp.Value);
        }

        public void Add(string key, object value)
        {
            this[key] = value;
        }
    }
}
