﻿using System.IO;

namespace Coscine.ProxyApi.Utils
{
    public interface IJson
    {
        T Deserialize<T>(Stream s);
        T Deserialize<T>(string s);
        string Serialize<T>(T t);
        void Serialize<T>(T t, Stream s);
    }
}
