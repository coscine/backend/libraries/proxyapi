﻿using System.Runtime.Serialization;

namespace Coscine.ProxyApi.Utils
{
    [DataContract]
    public class PropertyDescription
    {
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Type { get; set; }

        public PropertyDescription(string value, string type)
        {
            this.Value = value;
            this.Type = type;
        }
    }
}
