﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

#region DupFinder Exclusion
namespace Coscine.ProxyApi.Utils
{
    public interface IRestClient
    {
        T HttpGetJson<T>(Uri endpoint, string query, string user = null, string password = null);
        MemoryStream HttpGetRaw(Uri endpoint, string query);
        string HttpGetText(Uri endpoint, string query, string user = null, string password = null);
        XDocument HttpGetXml(Uri endpoint, string query);
        R HttpJson<R, B>(string method, Uri endpoint, string query, B body, string user = null, string password = null);
        T HttpPostJson<T>(Uri endpoint, string query, string body = null, string user = null, string password = null);
        string HttpPostJsonWithHeader(Uri endpoint, object body, Dictionary<string, string> headers);
        MemoryStream HttpPostRaw(Uri endpoint, string query, string body = null);
        string HttpPostText(Uri endpoint, string query, string body = null, string user = null, string password = null);
        string HttpPostTextOAuth(Uri endpoint, string body);
        string HttpPostXMLWithHeader(Uri endpoint, object body, Dictionary<string, string> headers);
        MemoryStream HttpRaw(string method, Uri endpoint, string query, string body = null, string user = null, string password = null);
        string HttpText(string method, Uri endpoint, string query, string body = null, string user = null, string password = null, string contentType = "application/json; charset=utf8", string accept = "application/json, */*", string userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64)", Dictionary<string, string> headers = null);
    }
}
#endregion
