﻿using System;

namespace Coscine.ProxyApi.Utils.Exceptions
{
    public class CustomException : Exception
    {
        public enum LogLevelEnum
        {
            Trace,
            Debug,
            Info,
            Warn,
            Error
        }

        public int ErrorCode { get; private set; }
        public LogLevelEnum LogLevel { get; private set; }
        public string InternalMessage { get; private set; }
        public bool CanBeCached { get; private set; }

        public CustomException(string message, int errorCode, LogLevelEnum logLevel = LogLevelEnum.Trace, string internalMessage = null, bool canBeCached = false)
            : base(message)
        {
            this.ErrorCode = errorCode;
            this.LogLevel = logLevel;
            this.InternalMessage = internalMessage;
            this.CanBeCached = canBeCached;
        }
    }
}
