﻿namespace Coscine.ProxyApi.Utils.Exceptions
{
    public class ApiRequestException : CustomException
    {
        public ApiRequestException(string message, int errorCode, LogLevelEnum logLevel = LogLevelEnum.Trace, string internalMessage = null, bool canBeCached = false)
            : base(message, errorCode, logLevel, internalMessage, canBeCached)
        {

        }
    }
}
