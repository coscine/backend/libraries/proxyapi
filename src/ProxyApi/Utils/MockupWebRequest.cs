﻿using System.Net;

namespace Coscine.ProxyApi.Utils
{
    public class MockupWebRequest : IWebRequest
    {
        public string ContentType => "application/json";

        public WebHeaderCollection Headers => new WebHeaderCollection();

        public string Method => "GET";

        public string Uri => "https://app.rwth-aachen.de";
    }
}
