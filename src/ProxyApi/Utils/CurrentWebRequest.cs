﻿using System.Net;

namespace Coscine.ProxyApi.Utils
{
    public class CurrentWebRequest : IWebRequest
    {
        public string ContentType
        {
            get
            {
                // return WebOperationContext.Current?.IncomingRequest?.ContentType;
                return "";
            }
        }

        public WebHeaderCollection Headers
        {
            get
            {
                // return WebOperationContext.Current?.IncomingRequest?.Headers;
                return null;
            }
        }

        public string Method
        {
            get
            {
                // return WebOperationContext.Current?.IncomingRequest?.Method;
                return "";
            }
        }

        public string Uri
        {
            get
            {
                // return WebOperationContext.Current?.IncomingRequest?.UriTemplateMatch?.RequestUri?.OriginalString;
                return "";
            }
        }
    }
}
