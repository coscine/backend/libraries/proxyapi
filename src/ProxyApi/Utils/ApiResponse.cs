﻿using System.Runtime.Serialization;

namespace Coscine.ProxyApi.Utils
{
    [DataContract]
    public class ApiResponse<T>
    {
        [DataMember]
        public T Data { get; set; }
        [DataMember]
        public int StatusCode { get; set; }
        [DataMember]
        public string StatusInfo { get; set; }
        [DataMember]
        public bool IsError { get; set; }

        public ApiResponse(T data, string statusText, int statusCode, bool error)
        {
            this.Data = data;
            this.StatusInfo = statusText;
            this.StatusCode = statusCode;
            this.IsError = error;
        }

        public ApiResponse()
        {
        }
    }
}
