﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Coscine.ProxyApi.Utils
{
    [DataContract]
    public class EpicData
    {
        [DataMember(Name = "idx", EmitDefaultValue = false)]
        public int Idx { get; set; }

        [DataMember(Name = "handle", EmitDefaultValue = false)]
        public int Handle { get; set; }

        [DataMember(Name = "type", EmitDefaultValue = false)]
        public string Type { get; set; }

        [DataMember(Name = "parsed_data", EmitDefaultValue = false)]
        public object ParsedData { get; set; }

        [DataMember(Name = "data", EmitDefaultValue = false)]
        public string Data { get; set; }

        [DataMember(Name = "timestamp", EmitDefaultValue = false)]
        public string Timestamp { get; set; }

        [DataMember(Name = "ttl_type", EmitDefaultValue = false)]
        public int TtlType { get; set; }

        [DataMember(Name = "ttl", EmitDefaultValue = false)]
        public int Ttl { get; set; }

        [DataMember(Name = "refs", EmitDefaultValue = false)]
        public List<EpicData> References { get; set; }

        [DataMember(Name = "privs", EmitDefaultValue = false)]
        public string Privileges { get; set; }

        [DataMember(Name = "epic-pid", EmitDefaultValue = false)]
        public string EpicPid { get; set; }
    }
}
