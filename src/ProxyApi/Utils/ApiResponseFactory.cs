﻿using Coscine.ProxyApi.Utils.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace Coscine.ProxyApi.Utils
{
    public static class ApiResponseFactory
    {
        private static OAuth2Client oAuth2Client = new OAuth2Client();
        public const string STATUS_OK = "ok";
        public const string STATUS_ERROR = "error";
        public const string STATUS_TOKEN_INVALID = "token invalid";

        public static ApiResponse<Dynamic> Create(Dynamic data)
        {
            var response = new ApiResponse<Dictionary<string, object>>(data.ToDictionaryDeep(), ApiResponseFactory.STATUS_OK, 0, false);

            var jss = new JsonSerializer();
            var responseJson = JsonConvert.SerializeObject(response);
            //if (HttpContext.Current != null && WebOperationContext.Current != null && WebOperationContext.Current.OutgoingResponse != null)
            //{
            //    HttpContext.Current.Response.Write(responseJson);
            //    HttpContext.Current.Response.ContentType = "application/json; charset=utf-8";
            //    WebOperationContext.Current.OutgoingResponse.SuppressEntityBody = true;
            //}

            return new ApiResponse<Dynamic>(data, ApiResponseFactory.STATUS_OK, 0, false); ;
        }

        public static ApiResponse<T> Create<T>(T data)
        {
            return new ApiResponse<T>(data, ApiResponseFactory.STATUS_OK, 0, false);
        }

        public static ApiResponse<T> CreateError<T>(string statusText, int statusCode, T data = default(T))
        {
            return new ApiResponse<T>(data, statusText, statusCode, true);
        }

        public static ApiResponse<T> CreateErrorFromException<T>(Exception ex)
        {
            if (ex is CustomException)
            {
                var customException = (CustomException)ex;
                return ApiResponseFactory.CreateError<T>(customException.Message, customException.ErrorCode);
            }
            else
            {
                return ApiResponseFactory.CreateErrorUnexpectedError<T>();
            }

        }

        public static ApiResponse<T> CreateErrorUnexpectedError<T>(T data = default(T))
        {
            return ApiResponseFactory.CreateError<T>(ApiResponseFactory.STATUS_ERROR, -1, data);
        }

        public static ApiResponse<T> CreateErrorTokenInvalid<T>(string scope = null, T data = default(T))
        {
            string error = $"Token invalid.";
            if (scope != null)
            {
                error = $"Token invalid: Token expired or scope '{scope}' is missing.";
            }

            return ApiResponseFactory.CreateError<T>(error, 1, data);
        }

        public static ApiResponse<T> CreateErrorTokenGroupMissing<T>(string[] missingGroups = null, T data = default(T))
        {
            string error = $"Token invalid: Permission groups are missing.";
            if (missingGroups != null)
            {
                error = $"Token invalid: Permission group(s) '{String.Join(",", missingGroups)}' missing.";
            }

            return ApiResponseFactory.CreateError<T>(error, 2, data);
        }

        public static ApiResponse<T> ValidateEduroamLogSecretAndExecute<T>(Func<T> f, string mode, string token, string modeTimestamp = null)
        {
            ApiRequestException notAllowedException = new ApiRequestException("You are not allowed to use EduroamLoggingAPI.", 548);

            switch (mode)
            {
                case "SIMPLE":

                    if (ConfigurationManager.AppSettings["EduroamLoggingApiSecret"] != token)
                    {
                        throw notAllowedException;
                    }
                    break;

                case "HASH1":

                    int now = Convert.ToInt32(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
                    int timestampForMode = int.Parse(modeTimestamp);

                    // when timestampForMode is placed in past or is set more far then one hour in future throw exception   
                    if (!(timestampForMode - now >= 0 && timestampForMode - now <= 3600))
                    {
                        throw notAllowedException;
                    }

                    SHA256 SHA256Obj = SHA256Managed.Create();

                    // hash a hundred times
                    string hashedToken = BitConverter.ToString(SHA256Obj.ComputeHash(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["EduroamLoggingApiSecret"] + modeTimestamp)));

                    for (int i = 0; i < 99; i++)
                    {
                        hashedToken = BitConverter.ToString(SHA256Obj.ComputeHash(Encoding.UTF8.GetBytes(hashedToken)));
                    }

                    hashedToken = hashedToken.Replace("-", "");

                    if (hashedToken != token)
                    {
                        throw notAllowedException;
                    }

                    break;

                default:

                    throw notAllowedException;
            }

            return ApiResponseFactory.Create(f());
        }
    }
}
