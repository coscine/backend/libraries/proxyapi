using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Coscine.ProxyApi.Utils
{
    public class EpicClient : IEpicClient
    {
        private readonly HttpClient _httpClient = new();

        public const int errorPrefix = 12000;

        private string ServiceEndpoint { get; set; }

        public string Prefix { get; private set; }

        public EpicClient(string url, string prefix, string user, string password)
        {
            ServiceEndpoint = url;
            Prefix = prefix;

            var byteArray = Encoding.ASCII.GetBytes($"{user}:{password}");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json, */*");
        }

        private void ReplacePayload(string suffix, List<EpicData> payload)
        {
            foreach (var entry in payload)
            {
                if (entry.ParsedData is string)
                {
                    entry.ParsedData = (entry.ParsedData as string)
                        .Replace("{{pid}}", HttpUtility.UrlEncode($"http://hdl.handle.net/{Prefix}/{suffix}"));
                }
            }
        }

        public EpicData Update(string suffix, List<EpicData> payload)
        {
            ReplacePayload(suffix, payload);

            using var requestContent = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");
            var result = EpicRequestWrapper(() => _httpClient.PutAsync(ServiceEndpoint + suffix, requestContent));
            if (result.IsSuccessStatusCode)
            {
                var content = result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<EpicData>(content);
            }
            return null;
        }

        public EpicData Create(List<EpicData> payload, string guid = null)
        {
            string suffix = guid;
            if (suffix is null)
            {
                suffix = Guid.NewGuid().ToString();
            }
            return Update(suffix, payload);
        }

        public IEnumerable<EpicData> Search(string searchUrl, int limit = 0, int page = 0)
        {
            var pidList = new List<EpicData>();

            // Handle additional query parameters
            var parameters = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(searchUrl))
            {
                parameters.Add("URL", searchUrl);
            }
            if (limit > 0)
            {
                parameters.Add("limit", limit.ToString());
            }
            if (page > 0)
            {
                parameters.Add("page", page.ToString());
            }
            // Build the request URL
            var url = new Uri(QueryHelpers.AddQueryString(ServiceEndpoint, parameters));
            // Execute the request
            var result = EpicRequestWrapper(() => _httpClient.GetAsync(url));
            if (result is not null && result.IsSuccessStatusCode)
            {
                var content = result.Content.ReadAsStringAsync().Result;
                content.Split('\n').ToList().ForEach(e => pidList.Add(new EpicData() { EpicPid = e }));
            }
            return pidList.Where(x => !string.IsNullOrWhiteSpace(x.EpicPid));
        }

        public IEnumerable<EpicData> SearchAll(string searchUrl, int limit = 1000, int startPage = 1)
        {
            var pidList = new List<EpicData>();
            var tempList = Search(searchUrl, limit, startPage);

            while (tempList.Any())
            {
                pidList.AddRange(tempList);
                startPage++;
                tempList = Search(searchUrl, limit, startPage);
            }

            return pidList;
        }

        public IEnumerable<EpicData> List(int limit = 0, int page = 0)
        {
            var pidList = new List<EpicData>();

            // Handle additional query parameters
            var parameters = new Dictionary<string, string>();
            if (limit > 0)
            {
                parameters.Add("limit", limit.ToString());
            }
            if (page > 0)
            {
                parameters.Add("page", page.ToString());
            }
            // Build the request URL
            var url = new Uri(QueryHelpers.AddQueryString(ServiceEndpoint, parameters));
            // Execute the request
            var result = EpicRequestWrapper(() => _httpClient.GetAsync(url));
            if (result.IsSuccessStatusCode)
            {
                var content = result.Content.ReadAsStringAsync().Result;
                content.Split('\n').ToList().ForEach(e => pidList.Add(new EpicData() { EpicPid = e }));
            }
            else
            {
                Console.WriteLine();
            }
            return pidList.Where(x => !string.IsNullOrWhiteSpace(x.EpicPid));
        }

        public IEnumerable<EpicData> ListAll(int limit = 1000, int startPage = 1)
        {
            var pidList = new List<EpicData>();
            var tempList = List(limit, startPage);

            while (tempList.Any())
            {
                pidList.AddRange(tempList);
                startPage++;
                tempList = List(limit, startPage);
            }

            return pidList;
        }

        public void Delete(string suffix)
        {
            var result = EpicRequestWrapper(() => _httpClient.DeleteAsync(ServiceEndpoint + suffix));
        }

        public IEnumerable<EpicData> Get(string suffix)
        {
            try
            {
                var pidList = new List<EpicData>();
                var result = EpicRequestWrapper(() => _httpClient.GetAsync(ServiceEndpoint + suffix));
                if (result.IsSuccessStatusCode)
                {
                    var content = result.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<List<EpicData>>(content);
                }
                return pidList;
            }
            catch (WebException e)
            {
                var response = (HttpWebResponse)e.Response;
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    return new List<EpicData>(0);
                }
                else
                {
                    throw e;
                }
            }
        }

        // Wrapper for requests to the Epic server since sometimes it gives a 500, however the request was valid
        public static HttpResponseMessage EpicRequestWrapper(Func<Task<HttpResponseMessage>> request)
        {
            var count = 0;
            var maxCount = 10;
            Exception lastException;
            do
            {
                try
                {
                    var requestTask = request();
                    return requestTask.Result;
                }
                catch (Exception e)
                {
                    lastException = e;
                    count++;
                }
            } while (count < maxCount);
            throw lastException;
        }
    }
}
