﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Coscine.ProxyApi.Utils
{
    public class RestClient : IRestClient
    {
        private static Json json = new Json();
        private static readonly string urlTemplateWithQuery = "{0}?{1}";
        private static readonly string urlTemplateWithoutQuery = "{0}";
        private static readonly string defaultAccept = "application/json, */*";
        private static readonly string defaultUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64)";
        private static readonly string defaultContentType = "application/json; charset=utf-8";

        private static WebResponse HttpRequest(
            Uri endpoint,
            string query = null,
            string method = "GET",
            string body = null,
            string user = null,
            string password = null,
            string contentType = "application/json; charset=utf-8",
            string accept = "application/json, */*",
            string userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64)",
            Dictionary<string, string> headers = null
            )
        {
            if (string.IsNullOrWhiteSpace(method))
            {
                method = "GET";
            }
            method = method.ToUpper();

            var template = urlTemplateWithQuery;
            if (string.IsNullOrWhiteSpace(query))
            {
                template = urlTemplateWithoutQuery;
                query = string.Empty;
            }

            var request = (HttpWebRequest)WebRequest.Create(string.Format(template, endpoint.OriginalString, query));

            if (headers != null)
            {
                foreach (var keyValuePair in headers)
                {
                    request.Headers.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }

            request.Method = method;
            request.ContentType = contentType;
            request.Accept = accept;
            request.UserAgent = userAgent;

            if (!string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(password))
            {
                request.Credentials = new NetworkCredential(user, password);
                request.PreAuthenticate = true;
            }

            if (!string.IsNullOrWhiteSpace(body) && method != "GET")
            {
                using (var stream = request.GetRequestStream())
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(body);
                    writer.Flush();
                }
            }
            else
            {
                request.ContentLength = 0;
            }

            WebResponse response = request.GetResponse();
            return response;
        }

        private string ReadResponse(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(responseStream))
            {
                string responseFromServer = reader.ReadToEnd();
                return responseFromServer.Trim();
            }
        }

        #region HTTP GET Shortcuts

        public MemoryStream HttpGetRaw(Uri endpoint, string query)
        {
            return HttpRaw("GET", endpoint, query);
        }

        public string HttpGetText(Uri endpoint, string query, string user = null, string password = null)
        {
            return HttpText("GET", endpoint, query, null, user, password);
        }

        public XDocument HttpGetXml(Uri endpoint, string query)
        {
            using (var response = HttpRequest(
                endpoint,
                query,
                "GET",
                null,
                null,
                null,
                "application/json; charset=utf-8",
                "application/xml, text/xml, */*"))
            {
                return XDocument.Parse(ReadResponse(response));
            }
        }

        public T HttpGetJson<T>(Uri endpoint, string query, string user = null, string password = null)
        {
            return HttpJson<T, object>("GET", endpoint, query, null, user, password);
        }

        #endregion

        #region HTTP POST Shortcuts

        public MemoryStream HttpPostRaw(Uri endpoint, string query, string body = null)
        {
            return HttpRaw("POST", endpoint, query, body);
        }

        public string HttpPostText(Uri endpoint, string query, string body = null, string user = null, string password = null)
        {
            return HttpText("POST", endpoint, query, body, user, password, "application/octet-stream");
        }

        public T HttpPostJson<T>(Uri endpoint, string query, string body = null, string user = null, string password = null)
        {
            return HttpJson<T, object>("POST", endpoint, query, body, user, password);
        }

        public string HttpPostJsonWithHeader(Uri endpoint, object body, Dictionary<string, string> headers)
        {
            var serializedBody = SerializeJSON(body);
            return HttpText("POST", endpoint, null, serializedBody, null, null, defaultContentType, defaultAccept, defaultUserAgent, headers);
        }

        public string HttpPostXMLWithHeader(Uri endpoint, object body, Dictionary<string, string> headers)
        {
            var serializedBody = SerializeXML(body);
            return HttpText("POST", endpoint, null, serializedBody, null, null, "text/xml", null, null, headers);
        }

        #endregion

        #region OAuth Shortcuts

        public string HttpPostTextOAuth(Uri endpoint, string body)
        {
            using (var response = HttpRequest(endpoint, null, "POST", body, null, null, "application/x-www-form-urlencoded"))
            {
                return ReadResponse(response);
            }
        }

        #endregion


        public MemoryStream HttpRaw(string method, Uri endpoint, string query, string body = null, string user = null, string password = null)
        {
            using var response = HttpRequest(endpoint, query, method, body, user, password);

            using Stream responseStream = response.GetResponseStream();
            var stream = new MemoryStream();
            responseStream.CopyTo(stream);
            stream.Flush();
            stream.Position = 0;
            return stream;
        }

        public string HttpText(string method, Uri endpoint, string query, string body = null, string user = null, string password = null, string contentType = "application/json; charset=utf8", string accept = "application/json, */*", string userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64)", Dictionary<string, string> headers = null)
        {
            using (var response = HttpRequest(endpoint, query, method, body, user, password, contentType, accept, userAgent, headers))
            {
                return ReadResponse(response);
            }
        }

        public R HttpJson<R, B>(string method, Uri endpoint, string query, B body, string user = null, string password = null)
        {
            var serializedBody = SerializeJSON(body);
            var text = HttpText(method, endpoint, query, serializedBody, user, password);
            return json.Deserialize<R>(text);
        }

        private string SerializeXML(object toSerialize)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            XmlSerializerNamespaces names = new XmlSerializerNamespaces();

            names.Add("", "");
            settings.OmitXmlDeclaration = true;

            MemoryStream ms = new MemoryStream();
            XmlWriter writer = XmlWriter.Create(ms, settings);
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(XmlNode));

            xmlSerializer.Serialize(writer, toSerialize, names);
            ms.Flush();
            ms.Seek(0, SeekOrigin.Begin);

            StreamReader sr = new StreamReader(ms);
            string xml = sr.ReadToEnd();

            writer.Close();

            return xml;
        }

        public static string CreateQueryString(KeyValuePair<string, string>[] queryParameters)
        {
            var queryString = System.Web.HttpUtility.ParseQueryString(string.Empty);

            foreach (var queryParam in queryParameters)
            {
                queryString[queryParam.Key] = queryParam.Value;
            }

            return queryString.ToString();
        }

        private string SerializeJSON(object toSerialize)
        {
            string serialized = null;

            if (toSerialize != null)
            {
                serialized = json.Serialize(toSerialize);
            }

            return serialized;
        }
    }
}
