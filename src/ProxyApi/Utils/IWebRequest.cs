﻿using System.Net;

namespace Coscine.ProxyApi.Utils
{
    public interface IWebRequest
    {
        string ContentType { get; }

        WebHeaderCollection Headers { get; }

        string Method { get; }

        string Uri { get; }
    }
}
