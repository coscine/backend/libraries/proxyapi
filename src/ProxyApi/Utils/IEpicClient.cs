﻿using System.Collections.Generic;

namespace Coscine.ProxyApi.Utils
{
    public interface IEpicClient
    {
        string Prefix { get; }
        EpicData Create(List<EpicData> payload, string guid = null);
        void Delete(string suffix);
        IEnumerable<EpicData> Get(string suffix);
        IEnumerable<EpicData> List(int count, int page);
        EpicData Update(string suffix, List<EpicData> payload);
    }
}
