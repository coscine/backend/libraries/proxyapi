﻿using Coscine.ProxyApi.Utils;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.ProxyApi
{
    public class MetadataApiConnector
    {

        public string RefreshToken { get; set; }
        private readonly string metadataApiUrl;
        private readonly string pidUrl;
        private readonly HttpClient client = new HttpClient();
        private readonly ApiConnector _apiConnector;

        public MetadataApiConnector(string refreshToken, string metadataApiUrl, ApiConnector apiConnector)
        {
            this.RefreshToken = refreshToken;
            this.metadataApiUrl = metadataApiUrl;
            this.pidUrl = metadataApiUrl.Substring(0, metadataApiUrl.IndexOf("api/v2") + "api/v2".Length) + "/eScience/PID/";
            this._apiConnector = apiConnector;
        }

        public Dictionary<string, List<PropertyDescription>> CreateMetadata(string currentPath, List<string> metadataUris, List<string> metadataTypes, Dictionary<string, PropertyDescription> metadataConstants, string longSchemaPath)
        {
            Dictionary<string, List<PropertyDescription>> metadataSet = new Dictionary<string, List<PropertyDescription>>();
            string[] schemaFolders = longSchemaPath.Substring(currentPath.Length).Trim('\\').Split('\\');

            for (int i = 0; i < metadataUris.Count; i++)
            {
                string propertyType = metadataTypes[i];
                string uri = metadataUris[i];
                string folderName = schemaFolders[i];
                if (!metadataSet.ContainsKey(uri))
                {
                    metadataSet.Add(uri, new List<PropertyDescription>());
                }
                metadataSet[uri].Add(new PropertyDescription(folderName, propertyType));
            }

            foreach (var constant in metadataConstants)
            {
                if (!metadataSet.ContainsKey(constant.Key))
                {
                    metadataSet.Add(constant.Key, new List<PropertyDescription>());
                }
                metadataSet[constant.Key].Add(constant.Value);
            }

            AddDefaultMetadata(metadataSet);

            return metadataSet;
        }

        private void AddDefaultMetadata(Dictionary<string, List<PropertyDescription>> metadataSet)
        {
            if (!metadataSet.ContainsKey("https://purl.org/rwth/md/form/1.0/publishMetadata"))
            {
                metadataSet.Add("https://purl.org/rwth/md/form/1.0/publishMetadata", new List<PropertyDescription>() { new PropertyDescription("https://purl.org/rwth/md/form/1.0/metadataIsProtected", "uri") });
            }
        }

        public string ParseMetadata(Dictionary<string, List<PropertyDescription>> metadataSet)
        {
            string parsedJson = JsonConvert.SerializeObject(metadataSet);
            return parsedJson;
        }

        public string UploadMetadata(string schemaUri, Dictionary<string, List<PropertyDescription>> metadataSet)
        {
            return UploadMetadata(schemaUri, ParseMetadata(metadataSet));
        }

        public string UploadMetadata(string schemaUri, string metadataJson)
        {
            string uploadUrl = $"{metadataApiUrl}StoreWithoutPid?schemaId={schemaUri}&token={_apiConnector.GetAccessToken(RefreshToken)}";

            var reqTask = SendPostRequest(uploadUrl, metadataJson);
            reqTask.Wait();
            var result = reqTask.Result;

            dynamic stuff = JsonConvert.DeserializeObject(result);

            return stuff.Data;
        }

        public bool DeleteMetadata(string pid)
        {
            string deleteUrl = $"{metadataApiUrl}Delete?pid={pid}&token={_apiConnector.GetAccessToken(RefreshToken)}";

            var reqTask = SendGetRequest(deleteUrl);
            reqTask.Wait();
            var httpResponse = reqTask.Result;

            return httpResponse.StatusCode == HttpStatusCode.OK;
        }

        public string GetOta(string pid)
        {
            string deleteUrl = $"{metadataApiUrl}GetOTA?pid={pid}&token={_apiConnector.GetAccessToken(RefreshToken)}";

            var reqTask = SendGetRequest(deleteUrl);
            reqTask.Wait();
            var httpResponse = reqTask.Result;

            var resTask = GetResponseString(httpResponse);
            resTask.Wait();
            var result = resTask.Result;

            dynamic stuff = JsonConvert.DeserializeObject(result);

            return stuff.Data;
        }

        public bool SetDataUrl(string ota, string pid, string dataUrl)
        {
            var suffix = pid.Substring(pid.LastIndexOf("/") + 1);

            string uploadUrl = $"{pidUrl}/{suffix}?ota={ota}&token={_apiConnector.GetAccessToken(RefreshToken)}";

            var reqTask = SendPutRequest(uploadUrl, "[{\"type\":\"URL\",\"parsed_data\":\"https://app.rwth-aachen.de/resolvehandle/?pid=" + pid + "\"},{\"type\":\"METAURL\",\"parsed_data\":\"https://moped.ecampus.rwth-aachen.de/proxy/api/v2/eScience/Metadata/Download?pid=" + pid + "\"},{\"type\":\"DATAURL\",\"parsed_data\":\"" + dataUrl + "\"}]");
            reqTask.Wait();
            var httpResponse = reqTask.Result;

            return httpResponse.StatusCode == HttpStatusCode.OK;
        }

        private async Task<HttpResponseMessage> SendPutRequest(string url, string values)
        {
            return await client.PutAsync(url, new StringContent(values));
        }

        private async Task<string> SendPostRequest(string url, string values)
        {
            var response = await client.PostAsync(url, new StringContent(values));
            return await response.Content.ReadAsStringAsync();
        }

        private async Task<string> GetResponseString(HttpResponseMessage message)
        {
            return await message.Content.ReadAsStringAsync();
        }

        private async Task<HttpResponseMessage> SendGetRequest(string url)
        {
            return await client.GetAsync(url);
        }

    }
}
