﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace Coscine.ProxyApi
{
    public class ApiConnector
    {
        private const string DEFAULT_OAUTH_ENDPOINT = "https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/token";
        private const string DEFAULT_OAUTH_SCOPE = "metadata.rwth";

        private readonly Uri oAuthUri;
        private readonly Uri codeEndpoint;
        private readonly Uri tokenEndpoint;
        private readonly string clientId;
        private readonly string scopes;

        private string deviceCode = null;

        public ApiConnector(string clientId = "")
        {
            oAuthUri = new Uri(DEFAULT_OAUTH_ENDPOINT);
            codeEndpoint = new Uri(oAuthUri, "code");
            tokenEndpoint = new Uri(oAuthUri, "token");

            this.clientId = (!string.IsNullOrWhiteSpace(clientId)) ? clientId : Environment.GetEnvironmentVariable("OAuth_ClientId", EnvironmentVariableTarget.Machine);
            scopes = DEFAULT_OAUTH_SCOPE;
        }

        public ApiConnector(Uri oAuthUri, string clientId, string scopes)
        {
            this.oAuthUri = oAuthUri;
            codeEndpoint = new Uri(oAuthUri, "code");
            tokenEndpoint = new Uri(oAuthUri, "token");

            this.clientId = clientId;
            this.scopes = scopes;
        }

        public string GetAccessToken(string refreshToken)
        {
            var postData = string.Format("client_id={0}&refresh_token={1}&grant_type=refresh_token", clientId, refreshToken);
            var response = PostRequest(tokenEndpoint, postData);
            var responseFromServer = ReadResponse(response);

            string accessToken = responseFromServer["access_token"];
            return accessToken;
        }

        public string GetVerifyUrl()
        {
            //-----------------------------------------------------------------
            // Create Token Request
            //-----------------------------------------------------------------
            string postData = string.Format("client_id={0}&scope={1}", clientId, scopes);

            WebResponse response = PostRequest(codeEndpoint, postData);
            Dictionary<string, string> responseFromServer = ReadResponse(response);

            //-----------------------------------------------------------------
            // Let The User Verify The Token
            //-----------------------------------------------------------------
            string verifyUrl = string.Format("{0}?q=verify&d={1}", responseFromServer["verification_url"], responseFromServer["user_code"]);

            deviceCode = responseFromServer["device_code"];

            return verifyUrl;
        }

        public string CreateNewRefreshToken()
        {

            string postData;
            WebResponse response = null;
            Dictionary<string, string> responseFromServer = null;
            if (deviceCode == null)
            {
                //-----------------------------------------------------------------
                // Create Token Request
                //-----------------------------------------------------------------
                postData = string.Format("client_id={0}&scope={1}", clientId, scopes);

                response = PostRequest(codeEndpoint, postData);
                responseFromServer = ReadResponse(response);
            }

            //-----------------------------------------------------------------
            // Get Access Token & Refresh Token
            //-----------------------------------------------------------------
            postData = string.Format("client_id={0}&code={1}&grant_type=device&device_name=UnitTests2015-07-17", clientId, (deviceCode != null) ? deviceCode : responseFromServer["device_code"]);

            int timeoutValue = 20;
            int count = 0;
            do
            {
                response = PostRequest(tokenEndpoint, postData);
                responseFromServer = ReadResponse(response);
                count++;
                Thread.Sleep(5100);
            }
            while (responseFromServer["status"].Contains("error") && count < timeoutValue);

            if (count == timeoutValue)
            {
                throw new TimeoutException("Authentification request timeouted!");
            }

            var refreshToken = responseFromServer["refresh_token"];
            return refreshToken;
        }

        public static Dictionary<string, string> ReadResponse(WebResponse response)
        {
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            responseStream.Close();
            response.Close();

            var jss = new JsonSerializer();
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(responseFromServer);
        }

        public static WebResponse PostRequest(Uri codeEndpoint, string postData)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            WebRequest codeRequest = WebRequest.Create(codeEndpoint);
            codeRequest.Method = "POST";
            codeRequest.ContentType = "application/x-www-form-urlencoded";
            codeRequest.ContentLength = byteArray.Length;
            Stream dataStream = codeRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse response = codeRequest.GetResponse();
            return response;
        }
    }
}
