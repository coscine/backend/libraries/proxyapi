﻿using Coscine.Configuration;
using Coscine.ProxyApi.Utils;
using System.Collections.Generic;

namespace Coscine.ProxyApi.TokenValidator
{
    public class MetadataValidator : ValidatorBase<MetadataContext>
    {
        protected override string Scope { get; } = System.Configuration.ConfigurationManager.AppSettings["OAuth2.MetadataScope"];
        protected override string ServiceId { get; } = System.Configuration.ConfigurationManager.AppSettings["OAuth2.MetadataServiceId"];

        public MetadataValidator(IWebRequest webRequest) : base(webRequest) { }
        public MetadataValidator(IWebRequest webRequest, IConfiguration configuration) : base(webRequest)
        {
            Scope = configuration.GetString("coscine/global/context/metadata/scope");
            ServiceId = configuration.GetString("coscine/global/context/metadata/serviceid");
        }
    }

    public class MetadataContext : ContextBase
    {
        public string UserId { get; set; }
        public List<string> Ikzs { get; set; }

        public override void FromGenericContext(OAuth2ClientContext context)
        {
            this.UserId = context.userId;
            this.Ikzs = context.groups;
            // IKZ 000000 for every user
            this.Ikzs.Add("000000");
        }
    }
}
