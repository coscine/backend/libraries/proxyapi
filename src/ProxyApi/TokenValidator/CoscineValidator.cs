﻿using Coscine.Configuration;
using Coscine.ProxyApi.Utils;

namespace Coscine.ProxyApi.TokenValidator
{
    public class CoscineValidator : ValidatorBase<CoscineContext>
    {
        protected override string Scope { get; } = System.Configuration.ConfigurationManager.AppSettings["OAuth2.CoscineScope"];
        protected override string ServiceId { get; } = System.Configuration.ConfigurationManager.AppSettings["OAuth2.CoscineServiceId"];

        public CoscineValidator(IWebRequest webRequest) : base(webRequest) { }
        public CoscineValidator(IWebRequest webRequest, IConfiguration configuration) : base(webRequest)
        {
            Scope = configuration.GetString("coscine/global/context/coscine/scope");
            ServiceId = configuration.GetString("coscine/global/context/coscine/serviceid");
        }
    }

    public class CoscineContext : ContextBase
    {

    }
}
