﻿using Coscine.ProxyApi.Utils;

namespace Coscine.ProxyApi.TokenValidator
{
    public class TokenValidator : ITokenValidator
    {
        private IWebRequest WebRequest { get; }

        public ValidatorBase<CoscineContext> Coscine { get; }
        public ValidatorBase<MetadataContext> Metadata { get; }

        public TokenValidator(IWebRequest webRequest, ValidatorBase<CoscineContext> coscine, ValidatorBase<MetadataContext> metadata)
        {
            WebRequest = webRequest;
            Coscine = coscine;
            Metadata = metadata;
        }
    }
}
