﻿using Coscine.ProxyApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.ProxyApi.TokenValidator
{
    public abstract class ValidatorBase<TypeContext> where TypeContext : ContextBase, new()
    {
        protected OAuth2Client oAuth2Client = new OAuth2Client();
        protected abstract string Scope { get; }
        protected abstract string ServiceId { get; }
        protected IWebRequest Request { get; }

        public ValidatorBase(IWebRequest request)
        {
            Request = request;
        }

        public virtual ApiResponse<TypeResponse> ValidateAndExecute<TypeResponse>(Func<TypeContext, TypeResponse> f, string token, params string[] groups)
        {
            var genericContext = oAuth2Client.GetParsedContextFromToken(token, this.Scope, this.ServiceId, Request.Uri, Request.Method);

            // Check if token was valid for requested scope
            if (!genericContext.isValid)
            {
                return ApiResponseFactory.CreateErrorTokenInvalid<TypeResponse>(this.Scope);
            }

            // Check if user has all required groups
            if (groups.Length > 0)
            {
                // Get a list of all required groups that are missing for the user
                IEnumerable<string> missingGroups = groups;
                if (genericContext.groups != null)
                {
                    missingGroups = groups.Except(genericContext.groups);
                }

                // If this list is not empty, deny access
                if (missingGroups.Count() > 0)
                {
                    return ApiResponseFactory.CreateErrorTokenGroupMissing<TypeResponse>(missingGroups.ToArray());
                }
            }

            var context = new TypeContext(); // Create instance
            context.FromGenericContext(genericContext); // Extract fields from generic context

            return ApiResponseFactory.Create(f(context));
        }

        public virtual ApiResponse<object> ValidateAndExecute(Action<TypeContext> f, string token, params string[] groups)
        {
            return ValidateAndExecute<object>((context) => { f(context); return null; }, token, groups);
        }
    }

    public abstract class ContextBase
    {
        public virtual void FromGenericContext(OAuth2ClientContext context)
        {
            // Empty method as default is to not parse anything
        }
    }
}
