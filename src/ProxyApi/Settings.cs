﻿namespace Coscine.ProxyApi
{
    public static class Settings
    {
        private static string oAuth2ContextUrlJson = "https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/context2";
        public static string OAuth2ContextUrlJson
        {
            get { return oAuth2ContextUrlJson; }
            set { oAuth2ContextUrlJson = value; }
        }
    }
}
