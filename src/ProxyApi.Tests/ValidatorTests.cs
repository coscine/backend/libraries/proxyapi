﻿using Coscine.Configuration;
using Coscine.ProxyApi.TokenValidator;
using Coscine.ProxyApi.Utils;
using NUnit.Framework;

namespace Coscine.ProxyApi.Tests
{
    [TestFixture]
    public class ValidatorTests
    {
        private readonly IConfiguration _configuration;
        private readonly ApiConnector _apiConnector;

        public ValidatorTests()
        {
            // default localhost
            _configuration = new ConsulConfiguration();
            _apiConnector = new ApiConnector();
        }


        [Test]
        public void ValidatorTest()
        {
            var refreshToken = _configuration.GetString("coscine/local/proxytest/refreshtoken");
            var accessToken = _apiConnector.GetAccessToken(refreshToken);

            var validator = new MetadataValidator(new MockupWebRequest(), _configuration);
            validator.ValidateAndExecute((context) =>
            {
                Assert.True(context != null);
            }, accessToken);
        }
    }
}
