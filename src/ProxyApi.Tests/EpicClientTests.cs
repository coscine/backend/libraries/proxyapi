﻿using Coscine.Configuration;
using Coscine.ProxyApi.Utils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.ProxyApi.Tests
{
    [TestFixture]
    public class EpicClientTests
    {
        private readonly IConfiguration _configuration;
        private readonly string _prefix;
        private readonly EpicClient _epicClient;
        private readonly string _guid;

        public EpicClientTests()
        {
            // default localhost
            _configuration = new ConsulConfiguration();

            _prefix = _configuration.GetString("coscine/global/epic/prefix");

            _epicClient = new EpicClient(
                _configuration.GetString("coscine/global/epic/url"),
                _prefix,
                _configuration.GetString("coscine/global/epic/user"),
                _configuration.GetString("coscine/global/epic/password")
            );
            _guid = Guid.NewGuid().ToString();
        }

        [TearDown]
        public void DeleteCreatedPid()
        {
            _epicClient.Delete(_guid);
        }

        [Test]
        public void TestCreate()
        {
            List<EpicData> list = GetPIDValues("pid/?pid={{pid}}");

            var epicData = _epicClient.Create(list, _guid.ToString());
            Assert.NotNull(epicData);
            Assert.IsTrue(epicData.EpicPid == _prefix + "/" + _guid.ToString());

            var epicDataList = _epicClient.Get(_guid.ToString());
            Assert.IsTrue(epicDataList.Any());
        }

        [Test]
        public void TestUpdate()
        {
            // Use old URL
            List<EpicData> list = GetPIDValues("coscine/apps/pidresolve/?pid={{pid}}");

            var epicData = _epicClient.Create(list, _guid.ToString());
            Assert.NotNull(epicData);
            Assert.IsTrue(epicData.EpicPid == _prefix + "/" + _guid.ToString());

            var epicDataList = _epicClient.Get(_guid.ToString());
            Assert.IsTrue(epicDataList.Any(e => e.ParsedData.ToString().Contains("coscine/apps/pidresolve/?pid=")));
            Assert.IsFalse(epicDataList.Any(e => e.ParsedData.ToString().Contains("pid/?pid=")));

            // Use new URL
            List<EpicData> listNew = GetPIDValues("pid/?pid={{pid}}");

            _epicClient.Update(_guid.ToString(), listNew);

            var epicDataListNew = _epicClient.Get(_guid.ToString());
            Assert.IsFalse(epicDataListNew.Any(e => e.ParsedData.ToString().Contains("coscine/apps/pidresolve/?pid=")));
            Assert.IsTrue(epicDataListNew.Any(e => e.ParsedData.ToString().Contains("pid/?pid=")));
        }

        private List<EpicData> GetPIDValues(string subpath)
        {
            var baseUrl = _configuration.GetStringAndWait(
                "coscine/local/app/additional/url",
                "https://coscine.rwth-aachen.de"
            );
            EpicData url = new EpicData
            {
                Type = "URL",
                ParsedData = _configuration.GetStringAndWait(
                    "coscine/global/epic/pid/url",
                    $"{baseUrl}/{subpath}"
                )
            };
            EpicData metaurl = new EpicData
            {
                Type = "METAURL",
                ParsedData = _configuration.GetStringAndWait(
                    "coscine/global/epic/pid/metaurl",
                    $"{baseUrl}/{subpath}"
                )
            };
            EpicData dataurl = new EpicData
            {
                Type = "DATAURL",
                ParsedData = _configuration.GetStringAndWait(
                    "coscine/global/epic/pid/dataurl",
                    $"{baseUrl}/{subpath}"
                )
            };
            return new List<EpicData>
            {
                url,
                metaurl,
                dataurl
            };
        }
    }
}
